﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pet : MonoBehaviour
{
    private float _trust = 0.3f;
    private float _independence = 0.0f;
    private float _stress = -0.1f;

    // GETTERS
    public float GetTrust()
    {
        return _trust;
    }

    public float GetIndependence()
    {
        return _independence;
    }

    public float GetStress()
    {
        return _stress;
    }

    // SETTERS
    public void SetTrust(float trust)
    {
        _trust = trust;
    }

    public void SetIndependence(float independence)
    {
        _independence = independence;
    }

    public void SetStress(float stress)
    {
        _stress = stress;
    }
}
