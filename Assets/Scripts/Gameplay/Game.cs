﻿using System.Collections;
using UnityEngine;

public class Game : MonoBehaviour
{
    [SerializeField] private StoryData _data;       // the story
    //[SerializeField] private RenderTexture _laptop;

    private TextDisplay _output;            // the computer screen
    private BeatData _currentBeat;          // the current page
    private WaitForSeconds _wait;

    private PlayerController _player;
    [SerializeField] private RenderTexture _mazeScreenTexture;

    private void Awake()
    {
        _output = GetComponentInChildren<TextDisplay>();        // ScreenText text
        _currentBeat = null;                            // no page is displayed
        _wait = new WaitForSeconds(0.5f);           // wait half a second - IS THIS USED?
        _player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    }

    private void Update()
    {
        if (_output.IsIdle)          // if screen text has finished
        {
            if (_currentBeat == null)   // and then if page(beat) is nothing
            {
                DisplayBeat(1);         // start first page
            }
            else
            {
                UpdateInput();      // else check for input
            }
        }
    }


    private void UpdateInput()
    {
        if (_player.GetMode() != PlayerController.Mode.Walk)
        {
            if (Input.GetKeyDown(KeyCode.Escape) && _player.GetMode() == PlayerController.Mode.Laptop)       //if escape pressed
            {
                if (_currentBeat != null)                    // and then if current page not empty
                {
                    if (_currentBeat.ID == 1)                   // and then if current page is first page
                    {
                        Application.Quit();                         // quit program
                    }
                    else
                    {
                        DisplayBeat(1);                             // else display page 1
                    }
                }
            }
            else if (Input.GetKeyDown(KeyCode.T))                         // look between the different screens
            {
                if (_player.GetMode() == PlayerController.Mode.Status)
                {
                    _player.SetMode(PlayerController.Mode.Laptop);
                }
                else
                {
                    _player.SetMode(PlayerController.Mode.Maze);
                }
            }
            else if (Input.GetKeyDown(KeyCode.R))
            {
                if (_player.GetMode() == PlayerController.Mode.Maze)
                {
                    _player.SetMode(PlayerController.Mode.Laptop);
                }
                else
                {
                    _player.SetMode(PlayerController.Mode.Status);
                }
            }
            else if(_player.GetMode() == PlayerController.Mode.Laptop)
            {
                KeyCode alpha = KeyCode.Alpha1;         // 1 at the top of the keyboard
                KeyCode keypad = KeyCode.Keypad1;       // or 1 on keypad

                for (int count = 0; count < _currentBeat.Decision.Count; ++count)   // for number of choices on current page
                {
                    if (alpha <= KeyCode.Alpha9 && keypad <= KeyCode.Keypad9)           // if key pressed is less than or equal to 9
                    {
                        if (Input.GetKeyDown(alpha) || Input.GetKeyDown(keypad))            // then if number key is pressed
                        {
                            ChoiceData choice = _currentBeat.Decision[count];                   // the choice made
                            if (choice.DisplayText == "Give Food")
                            {
                                _player.SetMode(PlayerController.Mode.GiveFoodView);
                            }
                            DisplayBeat(choice.NextID);                                         // get next page from choice and display it
                            break;
                        }
                    }

                    ++alpha;                    // check next key number
                    ++keypad;
                }
            }          
        }

        // SWITCH BETWEEN WALK (Q) AND SCREEN MODE (E)
        if (Input.GetKeyDown(KeyCode.E) && _player.GetMode() == PlayerController.Mode.Walk)
        {
            _player.SetMode(PlayerController.Mode.Laptop);
        }
        else if (Input.GetKeyDown(KeyCode.Q))
        {
            if(_player.GetMode() != PlayerController.Mode.GiveFoodView)
            {
                _player.SetMode(PlayerController.Mode.Walk);
            } else
            {
                _player.SetMode(PlayerController.Mode.Laptop);
                _player._mazeCamera.targetTexture = _mazeScreenTexture;
                _player._mazeCamera.orthographic = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
        }
    }


    private void DisplayBeat(int id)
    {
        BeatData data = _data.GetBeatById(id);      // gets the list of pages and finds the one with id
        StartCoroutine(DoDisplay(data));
        _currentBeat = data;                        // updates current page
    }

    private IEnumerator DoDisplay(BeatData data)
    {
        //CLEARS DISPLAY
        _output.Clear();                    // clears the text display

        while (_output.IsBusy)          // wait until text display is clear 
        {
            yield return null;
        }

        // PUTS STORY TEXT ON SCREEN
        _output.Display(data.DisplayText);      // display text of page

        while (_output.IsBusy)               // wait until text display is finished
        {
            yield return null;
        }

        // DISPLAYS CHOICES
        for (int count = 0; count < data.Decision.Count; ++count)           // for number of choices
        {
            ChoiceData choice = data.Decision[count];               // the choice made
            _output.Display(string.Format("{0}: {1}", (count + 1), choice.DisplayText));        // for each choice in list, display on screen

            while (_output.IsBusy)              // wait until text is displayed
            {
                yield return null;
            }
        }

        if (data.Decision.Count > 0)             // if there is a choice to make, display waiting cursor on bottom line
        {
            _output.ShowWaitingForInput();
        }
    }

}
