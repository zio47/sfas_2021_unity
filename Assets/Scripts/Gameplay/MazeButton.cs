﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeButton : MonoBehaviour
{
    [SerializeField] private Transform _door;
    private bool _open = false;
    private LayerMask _mouseLayer = 11;

    private void OnTriggerEnter(Collider mouse)
    {
        if (!_open && mouse.gameObject.layer == _mouseLayer)
        {
            _door.transform.position += new Vector3(12, 0, 0);
            _open = true;
        }

    }
}
