﻿using System;
using UnityEngine;

[Serializable]
public class ChoiceData         // the data for each choice. Each has:
{
    [SerializeField] private string _text;      // the choice's text
    [SerializeField] private int _beatId;       // the id of the page the choice leads to

    public string DisplayText { get { return _text; } }     // and getters for each variable
    public int NextID { get { return _beatId; } }
}
