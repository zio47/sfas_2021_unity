﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerAction
{
    public PlayerAction(float _trust, float _independence, float _stress) {
        Trust = _trust;
        Independence = _independence;
        Stress = _stress;

    }

    
    public float Trust { get; set; }
    public float Independence { get; set; }
    public float Stress { get; set; }
}
