﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private float _lookSpeed = 100.0f;
    private Transform _player;
    private float _xAxisClamp = 0.0f;
    private Transform _hand;

    private CharacterController _controller;
    private Vector3 _direction;
    [SerializeField] private float _speed = 10.0f;

    public enum Mode { Walk, Laptop, Maze, Status, GiveFoodView };   // whether the player can walk around(0), look at the laptop(1), look at maze(2) or look at status screen(3)
    [SerializeField] private Mode _mode = Mode.Walk;

    private Transform _screen;
    private Transform _mazeScreen;
    private Transform _statusScreen;

    [SerializeField] private float _velocity;
    [SerializeField] private float _gravity = 14.0f;

    public Camera _mazeCamera;

    // Start is called before the first frame update
    void Start()
    {
        _screen = GameObject.FindGameObjectWithTag("Laptop").transform;
        _mazeScreen = GameObject.FindGameObjectWithTag("MazeScreen").transform;
        _statusScreen = GameObject.FindGameObjectWithTag("StatusScreen").transform;
        _mazeCamera = GameObject.FindGameObjectWithTag("MazeCamera").GetComponent<Camera>();
        _player = GetComponent<Transform>();
        _controller = transform.parent.GetComponent<CharacterController>();
        _hand = GetComponentInChildren<Transform>();
        Cursor.lockState = CursorLockMode.Locked;
        _lookSpeed *= Time.deltaTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (_mode == Mode.Walk)
        {
            LookAround();
            Walk();
            Fall();
        }
        else if (_mode == Mode.Maze)
        {
            LookAtScreen(_mazeScreen);
        }
        else if (_mode == Mode.Status)
        {
            LookAtScreen(_statusScreen);
        }
        else if (_mode == Mode.Laptop)
        {
            PositionAt(_screen, 0.65f, 5.24f);
            LookAtScreen(_screen);
        } else if(_mode == Mode.GiveFoodView)
        {
            _mazeCamera.targetTexture = null;
            _mazeCamera.orthographic = true;
            Cursor.lockState = CursorLockMode.Confined;
        }
    }

    private void LookAround()
    {
        float mouseX = Input.GetAxis("Mouse X");        // gets mouse movement horizontal
        float mouseY = Input.GetAxis("Mouse Y");
        float rotX = mouseX * _lookSpeed;
        float rotY = mouseY * _lookSpeed;

        _xAxisClamp -= rotY;

        Vector3 rotation = _player.transform.rotation.eulerAngles;
        Vector3 rotHand = _hand.transform.rotation.eulerAngles;

        rotation.y += rotX;
        rotation.x -= rotY;
        rotation.z = 0;
        if (_xAxisClamp > 90)
        {
            _xAxisClamp = 90;
        }
        else if (_xAxisClamp < -90)
        {
            _xAxisClamp = -90;
        }
        _player.rotation = Quaternion.Euler(rotation);
    }

    private void PositionAt(Transform focus, float yOffset, float zOffset)
    {
        Vector3 screenPos = new Vector3(focus.position.x, focus.position.y - yOffset, focus.position.z - zOffset);
        _controller.transform.position = screenPos;
        LookAtScreen(focus);
    }

    private void LookAtScreen(Transform focus)
    {
        transform.LookAt(focus);
    }


    private void Walk()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        _direction = transform.right * x + transform.forward * z;
        _controller.Move(_direction * _speed * Time.deltaTime);
    }

    private void Fall()
    {
        if (_controller.isGrounded)
        {
            _velocity = -_gravity * Time.deltaTime;
        }
        else if (_velocity > -2)
        {
            _velocity -= _gravity * Time.deltaTime;
        }
        Vector3 moveVector = new Vector3(0, _velocity, 0);
        _controller.Move(moveVector * Time.deltaTime);

    }

    public Mode GetMode()
    {
        return _mode;
    }

    public void SetMode(Mode mode)
    {
        _mode = mode;
    }
}
