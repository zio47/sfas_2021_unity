﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScentData : MonoBehaviour
{
    [SerializeField] private string source;
    [SerializeField] private float timeRemaining = 10;
    [SerializeField] private bool isActive = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetSource(string label)
    {
        source = label;
    }

    public string GetSource()
    {
        return source;
    }

    public void ReduceTimeRemaining(float time)
    {
        timeRemaining -= time * Time.deltaTime;
    }

    public float GetTimeRemaining()
    {
        return timeRemaining;
    }

    public void SetTimeRemaining(float time)
    {
        timeRemaining = time;
    }

    public void setIsActive(bool active)
    {
        isActive = active;
        if (!isActive)
        {
            this.enabled = false;
        } else
        {
            this.enabled = true;
        }
    }
}
