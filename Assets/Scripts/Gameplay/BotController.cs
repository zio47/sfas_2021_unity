﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotController : MonoBehaviour
{
    private Transform _mouse;
   // [SerializeField] private MouseController _mouseData;

    // private GiveFood _giveFood = new GiveFood(0.8f, -0.2f, -0.5f);
    [SerializeField] private Transform _food;
    private PlayerController _player;

    [SerializeField] private int _foodCount = 2;

    void Start()
    {
        _mouse = GameObject.FindGameObjectWithTag("Mouse").transform;
        _player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        CameraMovement();
    }


    private void CameraMovement()
    {
        if(_player.GetMode() == PlayerController.Mode.GiveFoodView)
        {
            
            Vector3 mousePos = Input.mousePosition;
            mousePos.z = 10.0f; 
            Vector3 _spawnPoint = _player._mazeCamera.ScreenToWorldPoint(mousePos);
            transform.position = _spawnPoint;
            if (Input.GetMouseButtonDown(0) && _foodCount > 0)
            {
                Instantiate(_food, _spawnPoint, Quaternion.identity);
                _foodCount -= 1;
            }
        } else
        {
            transform.LookAt(_mouse);
            transform.RotateAround(_mouse.transform.position, Vector3.up, 20 * Time.deltaTime);
        }
        
    }

    /*private void SeeThroughWalls()
    {
        Vector3 neededCamPos = transform.parent.TransformPoint(dollyDir * maxDistance);
        RaycastHit hit; 
        if(Physics.Linecast(transform.parent.position, neededCamPos, out hit))
        {
            distance = Mathf.Clamp((hit.distance * 0.87f), minDistance, maxDistance);
        } else
        {
            distance = maxDistance;
        }
        transform.localPosition = Vector3.Lerp(transform.localPosition, dollyDir * distance, Time.deltaTime * smooth);
    }*/
}
