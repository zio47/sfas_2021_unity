﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Food : MonoBehaviour
{
    [SerializeField] private ScentData[] scents;
    private float timeFactor = 2f;
    private MouseController _mouse;
    

    // Start is called before the first frame update
    void Start()
    {
        _mouse = GameObject.FindGameObjectWithTag("Mouse").GetComponent<MouseController>();
    }

    // Update is called once per frame
    void Update()
    {
        ReduceScentTime();
    }

    public ScentData[] getScent()
    {
        return scents;
    }

    private void ReduceScentTime()
    {
        foreach (ScentData smellsLike in scents)
        {
            if (smellsLike.GetTimeRemaining() > 0 && Vector3.Distance(transform.position, _mouse.transform.position) < _mouse.getSmellRadius())
            {
                smellsLike.ReduceTimeRemaining(timeFactor);
                smellsLike.setIsActive(true);
            }
            else
            {
                if (smellsLike.GetTimeRemaining() < 0) {
                    smellsLike.SetTimeRemaining(0f);
                }
                smellsLike.setIsActive(false);                
            }
        }
    }

}
