﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class BeatData               // the data for each page. Each has:
{
    [SerializeField] private List<ChoiceData> _choices;     // a list of choices
    [SerializeField] private string _text;                  // the current page text
    [SerializeField] private int _id;                       // the page's id

    public List<ChoiceData> Decision { get { return _choices; } }   // and getters for variable
    public string DisplayText { get { return _text; } }
    public int ID { get { return _id; } }
}
