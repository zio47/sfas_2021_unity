﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseState : MonoBehaviour
{
    public enum Relationship { MoodyTeen, CoBefore, Independent, Loving, CoAfter, Stockholm, Aggressive, BadRelationship};
    [SerializeField] private int _trust = 0;
    [SerializeField] private int _independence = 75;
    [SerializeField] private int _stress = 0;
    [SerializeField] private Relationship _relationship = Relationship.Independent;

    [SerializeField] private int _maxTrust = 100;
    private int _minTrust = 0;
    private int _maxStress = 100;
    private int _minStress = 0;

    [SerializeField] private int _fear = 0;
    [SerializeField] private float _fearTimer = 2.0f;
    [SerializeField] private float _timer;

    [SerializeField] private Independent _independentValues;

    // Start is called before the first frame update
    void Start()
    {
        _independentValues = GetComponent<Independent>();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateRelationship();
        if(_fear >= 50 && _maxTrust > 55)
        {
            _timer += Time.deltaTime;
        }
        LongTermFear();
    }

    private void UpdateRelationship()
    {
        if(_trust < 50 && _independence < 50 && _stress < 50)               // MoodyTeen: Trust low, Independence low, Stress low
        {
            _relationship = Relationship.MoodyTeen;
        } else if (_trust >= 50 && _independence < 50 && _stress < 50)      // CoBefore: Trust high, Independence low, Stress low
        {
            _relationship = Relationship.CoBefore;
        } else if (_trust < 50 && _independence >= 50 && _stress < 50)      // Independent: Trust low, Independence high, Stress low
        {
            _relationship = Relationship.Independent;
            _independentValues.enabled = true;
        } else if (_trust >= 50 && _independence >= 50 && _stress < 50)     // Loving: Trust high, Independence high, Stress low
        {
            _relationship = Relationship.Loving;
        } else if (_trust < 50 && _independence < 50 && _stress >= 50)      // CoAfter: Trust low, Independence low, Stress high
        {
            _relationship = Relationship.CoAfter;
        } else if (_trust >= 50 && _independence < 50 && _stress >= 50)     // Stockholm: Trust high, Independence low, Stress high
        {
            _relationship = Relationship.Stockholm;
        } else if (_trust < 50 && _independence >= 50 && _stress >= 50)     // Aggressive: Trust low, Independence high, Stress high
        {
            _relationship = Relationship.Aggressive;
        } else if (_trust >= 50 && _independence >= 50 && _stress >= 50)    // BadRelationship: Trust high, Independence high, Stress high
        {
            _relationship = Relationship.BadRelationship;
        }
    }

    public void IncreaseTrust(int trustGain)
    {
        _trust += trustGain;
    }

    // IF FEAR IS MAINTAINED FOR LONG PERIODS OF TIME, THEN 
    // REDUCE THE MAX TRUST VALUE
    private void LongTermFear()         
    {
        if(_timer >= _fearTimer)
        {
            _maxTrust -= 5;
            _timer = 0;
        }
    }

    public Relationship GetRelationship()
    {
        return _relationship;
    }
}
