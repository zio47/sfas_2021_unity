﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Independent : MonoBehaviour
{
    private MouseState _relationship;

    // Start is called before the first frame update
    void Start()
    {
        _relationship = GetComponentInParent<MouseState>();
    }

    // Update is called once per frame
    void Update()
    {
        IsActive();
    }

    private void IsActive()
    {
        if(_relationship.GetRelationship() != MouseState.Relationship.Independent)
        {
            this.enabled = false;
        } 
    }
}
