﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class MouseController : MonoBehaviour
{
    private NavMeshAgent _mouse;
    [SerializeField] float _hungerLevel = 100.0f;
    private float _timeFactor = 10.0f;            // 1.0f CHANGE
    [SerializeField] private float _smellRadius = 15.0f;
    private bool _hasReachedTarget = true;
    private ScentData[] _scents;
    [SerializeField] private Collider _food;

    float _randomLocation;

    private readonly int _foodLayerMask = 9;

    [SerializeField] private float _timer;
    [SerializeField] private float _wanderTimer = 2.0f;

    private MouseState _feelings;

    private Transform _focus;
    private Vector3 _fixFocus;

    private List<string> _memory = new List<string>();      // NEED TO IMPLEMENT

    private void Start()
    {
        _mouse = GetComponent<NavMeshAgent>();
        _feelings = GetComponent<MouseState>();
        _focus = GameObject.FindGameObjectWithTag("Focus").transform;
    }

    private void Update()
    {
        _timer += Time.deltaTime;
        if (_hungerLevel < 90)
        {
            _food = SmellForFood();

            if (_food != null)
            {
                if (_hasReachedTarget)
                {
                    _scents = _food.GetComponent<Food>().getScent();
                }
                Move(_food.transform.position);
                _focus.position = _food.transform.position;
                _hasReachedTarget = false;
                if (Vector3.Distance(transform.position, _focus.position) < 2)
                {
                    IncreaseTrust(10, _scents);
                    Eat(_food);
                    _hasReachedTarget = true;
                }
            }
            else if(_food == null && _hasReachedTarget)
            {
                _randomLocation = Random.Range(0, 10);
                Wander();
                _focus.position = _fixFocus;
            }
        }
        else
        {
            _randomLocation = Random.Range(0, 10);
            Wander();
            _focus.position = _fixFocus;
        }
        ReduceHunger();
    }

    // AFTER A WANDER TIMER AMOUNT OF TIME, MOVE THE MOUSE TO A RANDOM LOCATION
    private void Wander()
    {
        if (_timer >= _wanderTimer)
        {
            _focus.position = RandomMove(_smellRadius - _randomLocation);
            _fixFocus = _focus.position;
            _mouse.SetDestination(_focus.position);
            _timer = 0;
        }
    }

    // MOVES THE MOUSE
    private void Move(Vector3 destination)
    {
        _mouse.SetDestination(destination);
    }

    // RETURNS A RANDOM LOCATION WITHIN THE NAVMESH
    private Vector3 RandomMove(float radius)
    {
        Vector3 direction = Random.insideUnitSphere * radius;
        direction += transform.position;
        NavMeshHit hit;
        Vector3 nextPos = Vector3.zero;
        if (NavMesh.SamplePosition(direction, out hit, radius, 1))
        {
            nextPos = hit.position;
        }
        return nextPos;
    }

    // INCREASES TRUST IF THE FOOD SMELLS LIKE THE PLAYER
    private void IncreaseTrust(int t, ScentData[] scents)
    {
        foreach (ScentData smellsLike in scents)
        {
            if (smellsLike.GetSource() == "Player")
            {
                _feelings.IncreaseTrust(t);
            }
        }
    }

    // GRADUALLY REDUCES HUNGER
    private void ReduceHunger()
    {
        if (_hungerLevel > 0)
        {
            _hungerLevel -= _timeFactor * Time.deltaTime;
        }
        else
        {
            _hungerLevel = 0;
        }
    }

    // RETURNS A LIST OF FOOD COLLIDER OBJECTS WITH SMELL RADIUS
    private Collider[] Smell(int layerMask)
    {
        // Get wind direction
        // Work out radius of scent based on wind direction
        int mask = 1 << layerMask;
        Collider[] objectsInRange = Physics.OverlapSphere(transform.position, _smellRadius, mask);
        return objectsInRange;
    }

    // FROM LIST OF FOOD COLLIDERS, PICKS THE FOOD THAT IS NEAREST THE MOUSE
    private Collider SmellForFood()
    {
        Collider[] foodInRange = Smell(_foodLayerMask);
        if (foodInRange.Length > 0)
        {
            Collider foodToEat = foodInRange[0];
            foreach (Collider food in foodInRange)
            {
                if (Vector3.Distance(transform.position, food.transform.position) < Vector3.Distance(transform.position, foodToEat.transform.position))
                {
                    foodToEat = food;
                }
            }
            return foodToEat;
        }
        return null;
    }

    // DESTROYS FOOD OBJECT AND RESETS HUNGER
    private void Eat(Collider food)
    {
        Destroy(food.gameObject);
        _hungerLevel = 100;
    }

    // RETURNS SMELL RADIUS
    public float getSmellRadius()
    {
        return _smellRadius;
    }

    private void React()
    {
        
    }


    // FOR DEBUGGING, DRAWS SMELL RADIUS
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(transform.position, _smellRadius);
    }
}
