﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Tutorial : MonoBehaviour
{
    private TMP_Text _instructions;
    private bool _moved = false;
    private bool _lookAtScreen = false;
    private bool _moveScreens = false;
    private bool _mazeInstruction = false;
    private PlayerController _player;
    [SerializeField] private Canvas _canvas;

    // Start is called before the first frame update
    void Start()
    {
        _instructions = GetComponent<TMP_Text>();
        _instructions.text = "Use the mouse to look and WASD or Arrow keys to move around.";
        _player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        _canvas = GetComponentInParent<Canvas>();
    }

    // Update is called once per frame
    void Update()
    {
        if ((Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0) && !_moved)
        {
            _instructions.text = "Very good. Now press E to look at the screen.";
            _moved = true;
        }
        else if (Input.GetKeyDown(KeyCode.E) && !_lookAtScreen && _moved)
        {
            _instructions.text = "Great. Choices will be made through this laptop using the number keys and you can view the other monitors using R and T. Give it a try.";
            _lookAtScreen = true;
        }
        else if ((Input.GetKeyDown(KeyCode.R) || Input.GetKeyDown(KeyCode.T)) && !_moveScreens && _lookAtScreen)
        {
            _instructions.text = "OK. Now follow the instructions on the laptop.";
            _moveScreens = true;
        }
        else if (_moveScreens && Input.anyKeyDown && _player.GetMode() != PlayerController.Mode.GiveFoodView)
        {
            _instructions.text = null;
        } else if(_player.GetMode() == PlayerController.Mode.GiveFoodView && !_mazeInstruction)
        {
            _canvas.worldCamera = _player._mazeCamera;
            _instructions.text = "Click to drop food. Press Q to go back.";
        } else
        {
            //_instructions.text = null;
        }
    }
}
