﻿using System.Collections;
using UnityEngine;
using TMPro;

public class TextDisplay : MonoBehaviour
{
    public enum State { Initialising, Idle, Busy }      // defines text display states

    private TMP_Text _displayText;          // the ScreenText text
    private string _displayString;
    private WaitForSeconds _shortWait;
    private WaitForSeconds _longWait;
    private State _state = State.Initialising;



    public bool IsIdle { get { return _state == State.Idle; } }     // returns idle state
    public bool IsBusy { get { return _state != State.Idle; } }     // returns busy state

    private void Awake()
    {
        _displayText = GetComponent<TMP_Text>();        // gets the ScreenText text component
        _shortWait = new WaitForSeconds(0.01f);          // speed of the text output
        _longWait = new WaitForSeconds(0.8f);

        _displayText.text = string.Empty;           // sets empty string
        _state = State.Idle;                    // sets state as idle
    }


    // ADDS TEXT TO DISPLAY
    private IEnumerator DoShowText(string text)
    {
        int currentLetter = 0;
        char[] charArray = text.ToCharArray();      // put given text into character array

        while (currentLetter < charArray.Length)        // while currentletter is less than size of text
        {
            _displayText.text += charArray[currentLetter++];    // add letter to display text
            yield return _shortWait;        // wait until text added
        }

        _displayText.text += "\n";      // display new line
        _displayString = _displayText.text;     // save display text
        _state = State.Idle;            // set state idle
    }


    private IEnumerator DoAwaitingInput()
    {
        bool on = true;         

        while (enabled)     // while text display is on
        {
            _displayText.text = string.Format( "{0}> {1}", _displayString, ( on ? "|" : " " ));
            on = !on;
            yield return _longWait;
        }
    }

    private IEnumerator DoClearText()
    {
        int currentLetter = 0;
        char[] charArray = _displayText.text.ToCharArray();

        while (currentLetter < charArray.Length)
        {
            if (currentLetter > 0 && charArray[currentLetter - 1] != '\n')
            {
                charArray[currentLetter - 1] = ' ';
            }

            if (charArray[currentLetter] != '\n')
            {
                charArray[currentLetter] = '_';
            }

            _displayText.text = charArray.ArrayToString();
            ++currentLetter;
            yield return null;
        }

        _displayString = string.Empty;
        _displayText.text = _displayString;
        _state = State.Idle;
    }

    public void Display(string text)
    {
        if (_state == State.Idle)
        {
            StopAllCoroutines();
            _state = State.Busy;
            StartCoroutine(DoShowText(text));
        }
    }

    public void ShowWaitingForInput()
    {
        if (_state == State.Idle)
        {
            StopAllCoroutines();
            StartCoroutine(DoAwaitingInput());
        }
    }

    public void Clear()
    {
        if (_state == State.Idle)
        {
            StopAllCoroutines();
            _state = State.Busy;
            StartCoroutine(DoClearText());
        }
    }
}
